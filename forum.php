<?php //engineroom things

include_once("db_config.php");

date_default_timezone_set("Europe/Helsinki");

$link = mysqli_connect($server,$user,$pswrd,$db);

if(!$link) {
    echo "Cannot connect to MySQL database! " . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    exit();
}

?>
<!-- html headers -->
<!DOCTYPE html><html lang='en'>

<head>
    <title>Assigment 5</title>
    <link rel='stylesheet' href='style.css'>
</head>

<body>
<p><h1>Public Forum</h1></p>
<div>

<?php // check if there is a post from form
if($_SERVER["REQUEST_METHOD"] == "POST") {
    if(empty($_POST['author'])){
        echo "No name";
        exit();
    }
    if(empty($_POST['msg'])){
        echo "No msg";
        exit();
    }  
    if($_FILES['image']['error'] == 4) {
        $image = NULL;
    } else {
        $image = $_FILES['image']['tmp_name'];
    }

    SaveMessage($link,$_POST['author'],$_POST['msg'], $image);
    ShowMessages($link);
} else { // if no post from form, lets show prev messages
    ShowMessages($link);
}

?>
<!-- footer of this page, search box and link to msg writing page-->
<hr>
  <form action='forum_search.php' method='post'>
    <table>
      <tr>
        <td class='bold'>Search</td>
        <td><input type='text' id='search' name='search' placeholder='from messages...' required></td>
        <td><input type='submit' value='Search'></td>
      </tr>
    </table>
<p><a href='forum_newmsg.php'>Write a message</a></p>
</div></body></html>


<?php
//-------------------------------------------SUM FUNCTIONS-------------------------------
//
// Function to save message
//
function SaveMessage ($link, $author, $message, $image) {
    $postdate = date("Y-m-d H:i:s"); // timestamp for message
    $imageID = NULL; // for possible image
    //First check if username is already in database
    //if not, create record for the username
    $sql = "SELECT * FROM users WHERE username LIKE ?";
    $stmt = mysqli_prepare($link,$sql);
    mysqli_stmt_bind_param($stmt, 's', $author);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    if(mysqli_fetch_assoc($result) == NULL) {
        $sql = "INSERT INTO users (username) VALUES (?);";
        $stmt = mysqli_prepare($link,$sql);
        mysqli_stmt_bind_param($stmt, 's', $author);
        mysqli_stmt_execute($stmt);
    }
    //If there is image, lets put it in
    if( $image != NULL ) {
        $imageHash = md5_file($image);
        $imageData = chunk_split(base64_encode(file_get_contents($image)));
        $sql = "INSERT INTO images (image, hash) VALUES (?,?);";
        $stmt = mysqli_prepare($link,$sql);
        mysqli_stmt_bind_param($stmt, 'ss', $imageData, $imageHash);
        mysqli_stmt_execute($stmt);
    } else {
        $imageHash = '0';
    }

    //Save the message
    $sql = "INSERT INTO messages (uid, message, timestamp, image_hash) VALUES ((SELECT users.uid FROM users where username = ?), ?, ?, ?);";
    $stmt = mysqli_prepare($link,$sql);
    mysqli_stmt_bind_param($stmt, 'ssss', $author, $message, $postdate, $imageHash);
    mysqli_stmt_execute($stmt);
}

//
// function to go through messages from DB
//
function ShowMessages ($link) {
    //Get all messages
    $sql = "SELECT * FROM messages";
    $stmt = mysqli_prepare($link,$sql);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);

    //fetch messages in an array
    $message_array = mysqli_fetch_all($result);

    // Get the rows for backwards looping
    // for loopping the array results (newest 2 first)
    $i = sizeof($message_array) - 1;
    echo "<table>";

    while ($i >= 0) {
        // Fetch username based on message related uid
        $sql = "SELECT users.username FROM users WHERE uid = ?";
        $stmt = mysqli_prepare($link,$sql);
        mysqli_stmt_bind_param($stmt, 'i', $message_array[$i][1]);
        mysqli_stmt_execute($stmt);

        $query = mysqli_stmt_get_result($stmt);
        $username = mysqli_fetch_assoc($query);
        // Print msg to webpage
        echo "<tr>
        <td class='boxed'>"  . $username['username'] . "<br>" .  $message_array[$i][3] . "</td>
        <td class='boxed' id='commentArea'>" . $message_array[$i][2];
        
        if ($message_array[$i][4] != 0) { // if there is image attached to msg
            $sql = "SELECT * FROM images WHERE hash = ?";
            $stmt = mysqli_prepare($link,$sql);
            mysqli_stmt_bind_param($stmt, 's', $message_array[$i][4]);
            mysqli_stmt_execute($stmt);

            $imageHashResult = mysqli_stmt_get_result($stmt);
            $imageHash = mysqli_fetch_row($imageHashResult);
            //echo var_dump ($imageHash);
            $imageData = base64_encode(base64_decode($imageHash[1]));
            echo "<hr><img src='data:image;base64," . $imageData . "'>";
        }
        
        echo "</td></tr><tr class='spacer'></tr>";
        
        $i--;
    }

    echo "</table>";
}
?>