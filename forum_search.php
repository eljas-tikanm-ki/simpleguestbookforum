<?php

include_once("db_config.php");

date_default_timezone_set("Europe/Helsinki");

$link = mysqli_connect($server,$user,$pswrd,$db);

if(!$link) {
    echo "Cannot connect to MySQL database! " . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    exit();
}
?>
<!-- html headers -->
<!DOCTYPE html><html lang='en'>

<head>
    <title>Assigment 5</title>
    <link rel='stylesheet' href='style.css'>
</head>

<body>
<p><h1>Public Forum</h1></p>
<div>
<?php // LETS SEARCH!!! :)
echo "<p><h3>Search results for \"" . $_POST['search'] . "\":</h3></p>";

if($_SERVER["REQUEST_METHOD"] == "POST") {

    if(empty($_POST['search'])){
        echo "Error. No string found!";
        exit();
    }
    // using %string% for search partial word from words ::DD
    // i u know what i mean :DD sql LIKE %...%
    $searchString = "%" . $_POST['search'] . "%"; 

    // Search the DB and bring back the goods

    $sql = "SELECT * FROM messages WHERE message LIKE ? ";
    $stmt = mysqli_prepare($link,$sql);
    mysqli_stmt_bind_param($stmt, 's', $searchString);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    //fetch messages in an array
    $message_array = mysqli_fetch_all($result);
    //Announce num of results and set the rows for backwards looping
    echo "<p><h3>Found " . sizeof($message_array) . " matches!</h3></p>";
    $i = sizeof($message_array) - 1;
    
    echo "<table>";
    // Going through the comments and picking up matches
    while($i >= 0) {
        // Fetch username based on message uid
        $sql = "SELECT users.username FROM users WHERE uid = ?";
        $stmt = mysqli_prepare($link,$sql);
        mysqli_stmt_bind_param($stmt, 'i', $message_array[$i][1]);
        mysqli_stmt_execute($stmt);

        $query = mysqli_stmt_get_result($stmt);
        $username = mysqli_fetch_assoc($query);
        // Print msg to webpage
        echo "<tr>
        <td class='boxed'>"  . $username['username'] . "<br>" .  $message_array[$i][3] . "</td>
        <td class='boxed' id='commentArea'>" . $message_array[$i][2];

        if ($message_array[$i][4] != 0) { // if there is image attached to msg
            $sql = "SELECT * FROM images WHERE hash = ?";
            $stmt = mysqli_prepare($link,$sql);
            mysqli_stmt_bind_param($stmt, 's', $message_array[$i][4]);
            mysqli_stmt_execute($stmt);

            $imageHashResult = mysqli_stmt_get_result($stmt);
            $imageHash = mysqli_fetch_row($imageHashResult);
            //echo var_dump ($imageHash);
            $imageData = base64_encode(base64_decode($imageHash[1]));
            echo "<hr><img src='data:image;base64," . $imageData . "'>";
        }

        echo "</td></tr><tr class='spacer'></tr>";
        
        $i--;
    }
    echo "</table>";
} else {
    echo "No search string!";
}
?>
<!-- footer of this page, links to write msg and read msg page-->
<hr>
<p><a href='forum.php'>Read messages</a></p>
<p><a href='forum_newmsg.php'>Write a message</a></p>
</div></body></html>