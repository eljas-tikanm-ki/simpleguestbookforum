<!DOCTYPE html><html lang='en'>

<head>
<title>Assigment 5</title>
<link rel='stylesheet' href='style.css'>
</head>

<body>
<p><h1>Public Forum</h1></p>

<div>
  <form action='forum.php' method='post' enctype='multipart/form-data'>
    <table>
      <tr>
        <td class='bold'>Name</td>
        <td><input type='text' id='author' name='author' placeholder='Your name...' required></td>
      </tr>
      <tr>
        <td  class='bold'>Comment</td>
        <td><textarea rows='20' id='msg' name='msg' cols='50' placeholder='Your comment...' required></textarea></td>
      </tr>
      <tr>
        <td class='bold'>Picture</td>
        <td><input type='file' id='image' name='image'> </td>
      </tr>
      <tr>
        <td></td>
        <td><input type='submit' value='Submit'></td>
      </tr>
    </table>
  </form>
  <hr>
  <form action='forum_search.php' method='post'>
    <table>
      <tr>
        <td class='bold'>Search for</td>
        <td><input type='text' id='search' name='search' placeholder='from messages...' required></td>
        <td><input type='submit' value='Search'></td>
      </tr>
    </table>
  </form>
  <p><a href='forum.php'>Read messages</a></p>
</div>
</body></html>